#!/bin/bash

echo "Take a while..."
status_s=$(curl --write-out %{http_code} --silent --output /dev/null sirius.planetfone.com.br)
status_n=$(curl --write-out %{http_code} --silent --output /dev/null saturno.planetfone.com.br)
status_t=$(curl --write-out %{http_code} --silent --output /dev/null terra.planetfone.com.br)
status_v=$(curl --write-out %{http_code} --silent --output /dev/null venus.planetarium.com.br)
status_p=$(curl --write-out %{http_code} --silent --output /dev/null plutao.planetarium.com.br)
status_c=$(curl --write-out %{http_code} --silent --output /dev/null gcchave.planetfone.com.br)

if [[ $status_s -eq 403 ]]; then
  echo "Sirius ativo."
else
  echo "Sirius inativo."
fi

if [[ $status_n -eq 200 ]]; then
  echo "Saturno ativo."
else
  echo "Saturno inativo."
fi

if [[ $status_t -eq 200 ]]; then
  echo "Terra ativo."
else
  echo "Terra inativo."
fi

if [[ $status_v -eq 200 ]]; then
  echo "Venus ativo."
else
  echo "Venus inativo."
fi

if [[ $status_p -eq 200 ]]; then
  echo "Plutao ativo."
else
  echo "Plutao inativo."
fi

if [[ $status_t -eq 200 ]]; then
  echo "Chave ativo."
else
  echo "Chave inativo."
fi
